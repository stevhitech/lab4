<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductCategories;
use App\Producer;

class ProducerController extends Controller
{
    //
    public function show(){
        $danh_muc = ProductCategories::all()->toArray();
        return view('show')->with('product_categories',$danh_muc);
    }
}
